package com.example.liarsdice.do_something;

/**
 * Created by Mike on 8/2/2016.
 */
public class Dice
{
    private int diceID;
    private int diceValue;
    private boolean diceLost;

    public Dice(int diceID)
    {
        this.diceID = diceID;
        this.diceValue = 0;
        this.diceLost = false;

    }

    // Instance variables



    // getters and setters

    public int getDiceID()
    {
        return diceID;
    }

    public int getDiceValue()
    {
        return diceValue;
    }

    // methods

    // throw dice method
    /*
    public void throwDice()
    {
        int diceValue = 0;
        while (diceValue == 0 || diceValue > 6) {
            diceValue = (int) (Math.random() * 10);
        }

    }*/

    // print Dice Values

    public void printDice()
    {
        System.out.println(diceID+","+diceValue);
    }


}
