package com.example.liarsdice.do_something;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    int numDice = 5;
    Dice[] dice = new Dice[numDice];



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    int diceThrow = 0;
    public void throwDice(View view)
    {
        diceThrow +=1;
        int diceValue = 0;
        while (diceValue == 0 || diceValue > 6) {
            diceValue = (int) (Math.random() * 10);
            displayDiceThrows(diceThrow);
            displayDice1(diceValue);
        }

    }

    private void displayDiceThrows(int j)
    {
        TextView number_Of_Throws =  (TextView) findViewById(R.id.Number_of_Throws);
        number_Of_Throws.setText(""+ j);

    }
    private void displayDice1(int i)
    {
        TextView dice1 = (TextView) findViewById(R.id.dice1);
        dice1.setText(""+i);
    }

}
